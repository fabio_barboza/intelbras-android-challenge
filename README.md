#Intelbras Android Challenge


As requested the application was created using the Kotlin language and is fetching the provided endpoint using the “Retrofit” Library.

## MVP

1. Create a private repository on Bitbucket and add @vitorsilveira as admin;

Done

2. Create a screen with the list of articles following the provided mockup;

Done

3. Ability to visualize the article’s content (all provided info);

Done

4. Ability to sort articles (date, title and author);

Done

5. Ability to mark articles as read/unread;

Done

6. Create a pull-request and assign it to @vitorsilveira.

Done


## Extras

- Tablet-adaptive layout;

Done

- Persistent info;

Done, using SQLLite to store and check is some news is reading

- Design tweaks: animation, icons, etc;

Done, created a sidebar menu with a menu Option "About"

- Manage network errors;

Done, at the application start, there is a internet conection verify.

- Image caching;

Done, using the “Glide” Library