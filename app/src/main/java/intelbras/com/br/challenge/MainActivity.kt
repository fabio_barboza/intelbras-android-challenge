package intelbras.com.br.challenge

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import intelbras.com.br.challenge.about.AboutActivity
import intelbras.com.br.challenge.news.NewsRepository
import intelbras.com.br.challenge.news.NewsDetailActivity
import intelbras.com.br.challenge.news.NewsListAdapter
import intelbras.com.br.challenge.news.NewsService
import intelbras.com.br.challenge.news.model.News
import intelbras.com.br.challenge.shared.CallbackResponse
import intelbras.com.br.challenge.shared.ClickListener
import intelbras.com.br.challenge.shared.RecyclerViewClickListener
import intelbras.com.br.challenge.shared.checkNetworkConnection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.card_news.view.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var recyclerViewNews: RecyclerView? = null

    private val newsRepository = NewsRepository(this, null, null, 1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        recyclerViewNews = findViewById(R.id.recyclerViewNews)
        val buttonNetwork: Button = findViewById(R.id.buttonNetwork)
        val textNetwork: TextView = findViewById(R.id.textNetwork)

        buttonNetwork.setOnClickListener {
            if (checkNetworkConnection(this)) {
                recyclerViewNews?.visibility = View.VISIBLE
                buttonNetwork.visibility = View.INVISIBLE
                textNetwork.visibility = View.INVISIBLE
                configureNews(this)
            } else {
                Toast.makeText(this@MainActivity, "No internet connection avaliable", Toast.LENGTH_SHORT).show()
            }
        }

        if (checkNetworkConnection(this)) {
            configureNews(this)
        } else {
            recyclerViewNews?.visibility = View.INVISIBLE
            buttonNetwork.visibility = View.VISIBLE
            textNetwork.visibility = View.VISIBLE
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionSortByTitle -> {
                NewsService.newsList = ArrayList(NewsService.newsList.sortedWith(compareBy({ it.title })))
                recyclerViewNews?.adapter = NewsListAdapter(NewsService.newsList, this)
                return true
            }
            R.id.actionSortByAuthor -> {
                NewsService.newsList = ArrayList(NewsService.newsList.sortedWith(compareBy({ it.authors })))
                recyclerViewNews?.adapter = NewsListAdapter(NewsService.newsList, this)
                return true
            }
            R.id.actionSortByDate -> {
                NewsService.newsList = ArrayList(NewsService.newsList.sortedWith(compareBy({ it.date })))
                recyclerViewNews?.adapter = NewsListAdapter(NewsService.newsList, this)
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun configureNews(context: Context) {
        recyclerViewNews!!.addOnItemTouchListener(RecyclerViewClickListener(context, recyclerViewNews!!, object : ClickListener {

            override fun onClick(view: View, position: Int) {
                if (!NewsService.newsList[position].read) {
                    newsRepository.addNews(NewsService.newsList[position])
                }
                NewsService.newsList[position].read = true
                view.textTitle.typeface = Typeface.DEFAULT
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra("news", NewsService.newsList[position])
                startActivity(intent)
            }

            override fun onLongClick(view: View?, position: Int) {}
        }))

        NewsService().list(object : CallbackResponse<List<News>> {
            override fun success(response: List<News>) {
                NewsService.newsList = arrayListOf()
                NewsService.newsList.addAll(response)
                NewsService.newsList.map {
                    if (newsRepository.findNews(it.title!!) != null) {
                        it.read = true
                    }
                }
                recyclerViewNews?.layoutManager = LinearLayoutManager(context)
                recyclerViewNews?.adapter = NewsListAdapter(NewsService.newsList, context)
            }
        })
    }
}
