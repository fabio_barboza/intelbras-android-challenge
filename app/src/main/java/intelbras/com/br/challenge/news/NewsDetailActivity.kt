package intelbras.com.br.challenge.news

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import intelbras.com.br.challenge.R
import intelbras.com.br.challenge.news.model.News

class NewsDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        val extra = intent.extras

        val news = extra!!.getSerializable("news") as News

        val textAuthors: TextView = findViewById(R.id.textAuthors)
        val textDate: TextView = findViewById(R.id.textDate)
        val textContent: TextView = findViewById(R.id.textContent)
        val textTitle: TextView = findViewById(R.id.textTitle)
        val textWebsite: TextView = findViewById(R.id.textWebsite)
        val textTags: TextView = findViewById(R.id.textTags)
        val imageNews: ImageView = findViewById(R.id.imageNews)

        textAuthors.text = news.authors
        textDate.text = news.date
        textContent.text = news.content
        textTitle.text = news.title
        textWebsite.text = "Source: ${news.website}"
        textTags.text = "Tags: ${news.tags.map { it.label }.joinToString("   ")}"

        Glide.with(this).load(news.imageUrl).into(imageNews)
    }
}
