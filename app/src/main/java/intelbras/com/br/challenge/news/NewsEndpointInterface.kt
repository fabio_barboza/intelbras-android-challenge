package intelbras.com.br.challenge.news

import intelbras.com.br.challenge.news.model.News
import retrofit2.Call
import retrofit2.http.GET

interface NewsEndpointInterface {

    @GET("desafio")
    fun list(): Call<List<News>>

}