package intelbras.com.br.challenge.news

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import intelbras.com.br.challenge.R
import intelbras.com.br.challenge.news.model.News
import kotlinx.android.synthetic.main.card_news.view.*


class NewsListAdapter(private val newsList: List<News>,
                      private val context: Context) : Adapter<NewsListAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val news = newsList[position]
        holder?.let {
            it.bindView(news)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_news, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(news: News) {
            val title = itemView.textTitle
            val authors = itemView.textAuthors
            val date = itemView.textDate
            val image = itemView.imageNews

            if (news.read) {
                itemView.textTitle.typeface = Typeface.DEFAULT
            }

            title.text = news.title
            authors.text = news.authors
            date.text = news.date
            Glide.with(itemView).load(news.imageUrl).into(image)
        }

    }

}
