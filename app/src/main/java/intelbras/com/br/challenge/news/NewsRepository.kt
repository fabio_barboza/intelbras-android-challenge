package intelbras.com.br.challenge.news

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import intelbras.com.br.challenge.news.model.News

class NewsRepository(context: Context, name: String?,
                     factory: SQLiteDatabase.CursorFactory?, version: Int) : SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_PRODUCTS_TABLE = "CREATE TABLE IF NOT EXISTS $TABLE_NEWS ($COLUMN_TITLE TEXT)"
        db.execSQL(CREATE_PRODUCTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int,
                           newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NEWS")
        onCreate(db)
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "Challenge.db"
        const val TABLE_NEWS = "news"

        const val COLUMN_TITLE = "title"    }

    fun addNews(news: News) {

        val values = ContentValues()
        values.put(COLUMN_TITLE, news.title)

        val db = this.writableDatabase

        db.insert(TABLE_NEWS, null, values)
        db.close()
    }

    fun findNews(title: String): News? {
        val query = "SELECT * FROM $TABLE_NEWS WHERE $COLUMN_TITLE = \"$title\""

        val db = this.writableDatabase

        val cursor = db.rawQuery(query, null)

        var news: News? = null

        if (cursor.moveToFirst()) {
            cursor.moveToFirst()
            news = News(title = cursor.getString(0))
            cursor.close()
        }

        db.close()
        return news
    }
}