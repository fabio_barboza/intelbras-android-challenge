package intelbras.com.br.challenge.news

import android.util.Log
import intelbras.com.br.challenge.news.model.News
import intelbras.com.br.challenge.shared.CallbackResponse
import intelbras.com.br.challenge.shared.RetrofitInitializer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsService {

    private val newsApi = RetrofitInitializer().newsApi()

    companion object {
        @JvmStatic
        var newsList: ArrayList<News> = arrayListOf()
    }

    fun list(callbackResponse: CallbackResponse<List<News>>) {
        newsApi.list().enqueue(object : Callback<List<News>?> {
            override fun onResponse(call: Call<List<News>?>?,
                                    response: Response<List<News>?>?) {
                response?.body()?.let {
                    val newsList: List<News> = it
                    callbackResponse.success(newsList)
                }
            }

            override fun onFailure(call: Call<List<News>?>?, t: Throwable?) {
                Log.e("Error on load data", t?.message)
            }
        })
    }
}