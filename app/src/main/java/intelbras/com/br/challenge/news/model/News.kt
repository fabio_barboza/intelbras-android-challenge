package intelbras.com.br.challenge.news.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

data class News(
        var title: String? = null,
        var website: String? = null,
        var authors: String? = null,
        var content: String? = null,
        var date: String? = null,
        @SerializedName("image_url")
        var imageUrl: String? = null,
        var tags: ArrayList<NewsTag> = arrayListOf(),
        var read: Boolean = false
) : Serializable