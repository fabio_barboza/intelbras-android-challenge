package intelbras.com.br.challenge.news.model

import java.io.Serializable

data class NewsTag(var id: Int? = null, var label: String? = null) : Serializable