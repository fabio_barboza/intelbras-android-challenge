package intelbras.com.br.challenge.shared

interface CallbackResponse<T> {
    fun success(response: T)
}