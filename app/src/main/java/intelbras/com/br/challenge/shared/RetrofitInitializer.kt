package intelbras.com.br.challenge.shared

import intelbras.com.br.challenge.news.NewsEndpointInterface
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

        private val retrofit = Retrofit.Builder()
                .baseUrl("http://ec2-34-215-199-111.us-west-2.compute.amazonaws.com:5000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        fun newsApi() = retrofit.create(NewsEndpointInterface::class.java)

}